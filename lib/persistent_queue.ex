defmodule PersistentQueue do
  use Application

  def start(_, _) do
    QueueSupervisor.start_link([])
  end
end
