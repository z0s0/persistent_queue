defmodule Queue do
  use GenServer

  @dets_filename :persistent_queue

  def start_link do
    GenServer.start_link(__MODULE__, [], name: :queue)
  end

  def init(_) do
    with {:error, _} <- :dets.is_dets_file(@dets_filename) do
      setup_state()
    else
      _ ->
        {:ok, ref} = :dets.open_file(@dets_filename)
        get_table_state(ref)
    end
  end

  def enqueue(val), do: GenServer.call(:queue, {:enqueue, val})
  def dequeue,      do: GenServer.call(:queue, :dequeue)
  def get_state,    do: GenServer.call(:queue, :get_state)

  def handle_call(:get_state, _from, state), do: {:reply, state, state}

  def handle_call({:enqueue, val}, _from, {[front_stack: fs], back_stack}) do
    {
      :reply,
      val,
      {
       [front_stack: [val | fs]],
       back_stack
      }
    }
  end

  def handle_call(:dequeue, _from, {[front_stack: []], [back_stack: [h|t]]}) do
    {
      :reply,
      h,
      {
       [front_stack: []],
       [back_stack: t]
      }
    }
  end
  def handle_call(:dequeue, _from, {[front_stack: [_h1|_t1] = fs], [back_stack: [h2,t2]]}) do
    {
      :reply,
      h2,
      {
       [front_stack: fs],
       [back_stack: t2]
      }
    }
  end
  def handle_call(:dequeue, _from, {[front_stack: [h|t] = fs], [back_stack: []]}) do
    [deq_val | reversed_rest] = Enum.reverse(fs)
    {
      :reply,
      deq_val,
      {
        [front_stack: []],
        [back_stack: reversed_rest]
      }
    }
  end

  def handle_info(:shutdown, state), do: {:stop, :shutdown, state}

  def terminate(:shutdown, {[front_stack: fs], [back_stack: bs]}) do
    {:ok, ref} = :dets.open_file(@dets_filename)
    :dets.insert(ref, front_stack: fs)
    :dets.insert(ref, back_stack: bs)
    :dets.close(ref)
  end

  defp setup_state do
    :dets.open_file(@dets_filename, type: :set)
    :dets.insert(@dets_filename, front_stack: [])
    :dets.insert(@dets_filename, back_stack: [])

    get_table_state(@dets_filename)
  end

  defp get_table_state(table_name_or_ref) do
    {
      :ok,
    {
      :dets.lookup(table_name_or_ref, :front_stack),
      :dets.lookup(table_name_or_ref, :back_stack)
    }
    }
  end
end