defmodule QueueSupervisor do
  use Supervisor

  def start_link(_) do
    Supervisor.start_link(__MODULE__, [], name: :queue_restarter)
  end

  def init(_) do
    children = [
      worker(Queue, [], restart: :permanent)
    ]

    supervise(
     children,
     max_restarts: 20,
     max_seconds: 2,
     strategy: :one_for_one
    )
  end
end